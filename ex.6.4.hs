euclid :: Int -> Int -> Int
euclid n 0 = n
euclid 0 m = m
euclid n m | n == m = n
           | otherwise = euclid (a - b) b
           where 
              a = max n m
              b = min n m