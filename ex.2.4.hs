--last [x] = x
--last (x:y:xs) = last (y:xs) -- or last (x:xs) = last xs

last l = head (drop (length l - 1) l)

last l = l !! (length l - 1)

last l = head (reverse l)