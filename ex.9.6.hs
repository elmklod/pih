data Op = Add | Sub | Mul | Div | Exp
          deriving Eq

instance Show Op where
   show Add = "+"
   show Sub = "-"
   show Mul = "*"
   show Div = "/"
   show Exp = "^"

valid :: Op -> Integer -> Integer -> Bool
valid Add x y = x <= y --  it is unfortunate that one can never get rid of duplicates of expression that operate on 2 equal value in Mul and Add, would shorten solutions amount quit a bit
valid Sub x y = x > y
valid Mul x y = x /= 1 && y /= 1 && x <= y
valid Div x y = y /= 1 && x `mod` y == 0
valid Exp x y = x /= 1 && y /= 1 -- the first restriction is to remove cruft like 1^n, the second is to remove a^1 == a.

apply :: Op -> Integer -> Integer -> Integer
apply Add x y = x + y
apply Sub x y = x - y
apply Mul x y = x * y
apply Div x y = x `div` y
apply Exp x y = x ^ y

data Expr = Val Integer | App Op Expr Expr

instance Show Expr where
   show (Val n) = show n
   show (App o l r) = brak l ++ show o ++ brak r
                      where
                         brak (Val n) = show n
                         brak e = "(" ++ show e ++ ")"

perms :: [a] -> [[a]]
perms [] = [[]]
perms (x:xs) = concat (map (interleave x) (perms xs))

interleave :: a -> [a] -> [[a]]
interleave x [] = [[x]]
interleave x (y:ys) = (x:y:ys) : map (y:) (interleave x ys)

subs :: [a] -> [[a]]
subs [] = [[]]
subs (x:xs) = yss ++ map (x:) yss
              where yss = subs xs

choices :: [a] -> [[a]]
choices xs = [p | s <- subs xs, p <- perms s]

split :: [a] -> [([a],[a])]
split [] = []
split [_] = []
split (x:xs) = ([x],xs) : [(x:ls,rs) | (ls,rs) <- split xs]

ops :: [Op]
ops = [Add,Sub,Mul,Div,Exp]

type Result = (Expr,Integer)

results :: [Integer] -> [Result]
results [] = []
results [n] = [(Val n,n) | n > 0]
results ns = [res | (ls,rs) <- split ns,
                    lx <- results ls,
                    ry <- results rs,
                    res <- combine' lx ry]

combine' :: Result -> Result -> [Result]
combine' (l,x) (r,y) = [(App o l r, apply o x y) | o <- ops, valid o x y]

solutions' :: [Integer] -> Integer -> [Expr]
solutions' ns n = case [r | ns' <- choices ns, r <- results ns'] of
                     [] -> []
                     ((e,m):xs) -> approximate [e] (abs (m - n)) xs -- order is not placed here before approximate
                  where
                     approximate es d [] = order es -- placed here to save some stack space, haha
                     approximate es d ((e,m):xs) | abs (m - n) > d = approximate es d xs
                                                 | abs (m - n) == d = approximate (e:es) d xs
                                                 | otherwise = approximate [e] (abs (m - n)) xs

order :: [Expr] -> [Expr]
order = map (\(e,_) -> e) . sort . map (\e -> (e, complexity e))

instance {-# OVERLAPPING #-} Eq (Expr, Int) where
   (_,n) == (_,m) = n == m

instance {-# OVERLAPPING #-} Ord (Expr, Int) where
   (_,n) <= (_,m) = n <= m

sort :: Ord a => [a] -> [a]
sort [] = []
sort (x:xs) = l ++ [x] ++ g
              where
                 l = sort [a | a <- xs, a <= x]
                 g = sort [a | a <- xs, a > x]

-- the real complexity of any operation of course depends on its operands values
complexity :: Expr -> Int
complexity (Val _) = 0
complexity (App op l r) = find op ops_complexities + complexity l + complexity r

type Assoc a b = [(a,b)]

find :: Eq k => k -> Assoc k v -> v
find k t = head [v | (k', v) <- t, k' == k]

ops_complexities :: Assoc Op Int
ops_complexities = [(Add, 10), (Sub, 20), (Mul, 100), (Div, 200), (Exp, 1000)]

main :: IO ()
main = print (solutions' [1,2,3,4] 16)