fac :: Int -> Int
fac 0 = 1
fac n | n > 0 = n * fac (n-1)
      | otherwise = n

-- fac (-1) = (-1) * fac (-2) = (-1) * ((-2) * fact (-3)) = ...
-- It will never terminate and will blow stack (the base case fac 0 is never reached)