adder :: IO ()
adder = do putChar '\n'
           putStr "How many numbers? "
           ds <- getLine
           rec (read ds) 0
        where
--           readInt :: String -> Int
--           readInt = read
           rec 0 s = putStrLn ("The total is " ++ (show s))
           rec n s = do ds <- getLine
                        rec (n - 1) (s + (read ds))
           -- It is possible to insert guard | n > 0 to deal with negative n but I don't think that is necessary
