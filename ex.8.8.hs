data Prop = Const Bool
          | Var Char
          | Not Prop
          | And Prop Prop
          | Imply Prop Prop
          | Or Prop Prop
          | Equivalent Prop Prop

type Assoc k v = [(k,v)]

type Subst = Assoc Char Bool

eval :: Subst -> Prop -> Bool
eval _ (Const b) = b
eval s (Var x) = find x s
eval s (Not p) = not (eval s p)
eval s (And p q) = eval s p && eval s q
eval s (Imply p q) = eval s p <= eval s q
eval s (Or p q) = eval s p || eval s q
eval s (Equivalent p q) = eval s p == eval s q

vars :: Prop -> [Char]
vars (Const _) = []
vars (Var x) = [x]
vars (Not p) = vars p
-- vars (_ p q) = vars p ++ vars q a shame I need TH to do this
vars (And p q) = vars p ++ vars q
vars (Imply p q) = vars p ++ vars q
vars (Or p q) = vars p ++ vars q
vars (Equivalent p q) = vars p ++ vars q

bools :: Int -> [[Bool]]
bools 0 = [[]]
bools n = map (False:) bss ++ map (True:) bss
          where bss = bools (n-1)


substs :: Prop -> [Subst]
substs p = map (zip vs) (bools (length vs))
           where vs = rmdups (vars p)

isTaut :: Prop -> Bool
isTaut p = and [eval s p | s <- substs p]

find :: Eq k => k -> Assoc k v -> v
find k t = head [v | (k', v) <- t, k == k']

rmdups :: Eq a => [a] -> [a]
rmdups [] = []
rmdups (x:xs) = x : (rmdups (filter (/= x) xs))

p1 :: Prop -- False
p1 = And (Var 'A') (Not (Var 'A'))
p2 :: Prop -- True
p2 = Imply (And (Var 'A') (Var 'B')) (Var 'A')
p3 :: Prop -- False
p3 = Imply (Var 'A') (And (Var 'A') (Var 'B'))
p4 :: Prop -- True
p4 = Imply (And (Var 'A')
                (Imply (Var 'A') (Var 'B'))) 
           (Var 'B')
p5 :: Prop -- True
p5 = Or (Var 'A') (Not (Var 'A'))
p6 :: Prop -- False
p6 = Imply (Or (Var 'A') (Var 'B')) (Var 'A')
p7 :: Prop -- True
p7 = Imply (Var 'A') (Or (Var 'A') (Var 'B'))
p8 :: Prop -- False
p8 = Imply (Or (Var 'A')
                (Imply (Var 'A') (Var 'B'))) 
           (Var 'B')
p9 :: Prop -- True
p9 = Equivalent (And (Var 'A') (Var 'B'))
                (Not (Or (Not (Var 'B')) (Not (Var 'A'))))
p10 :: Prop -- False
p10 = Equivalent (Imply (Var 'A') (Var 'B'))
                 (Imply (Var 'B') (Var 'A'))
p11 :: Prop -- True
p11 = Equivalent (Imply (Var 'A') (Var 'B'))
                 (Imply (Not (Var 'B')) (Not (Var 'A')))
p12 :: Prop -- True
p12 = Equivalent (Equivalent (Var 'A') (Var 'B'))
                 (And (Imply (Var 'A') (Var 'B'))
                      (Imply (Var 'B') (Var 'A')))
p13 :: Prop -- True
p13 = Not (Or (And (Const False) (Var 'D')) (And (And (Var 'C') (And (Var 'A') (Var 'B')))
                                                 (And (And (Var 'B') (Not (Var 'C'))) (Var 'A'))))