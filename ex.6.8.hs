halve :: [a] -> ([a],[a])
halve xs = splitAt (length xs `div` 2) xs
{-         (take l xs, drop l xs)
           where l = length xs `div` 2
-}

merge :: Ord a => [a] -> [a] -> [a]
merge [] ys = ys
merge xs [] = xs
merge (x:xs) (y:ys) | x <= y = x : (merge xs (y:ys))
                    | otherwise = y : (merge (x:xs) ys)

msort :: Ord a => [a] -> [a]
msort [] = []
msort [x] = [x]
msort xs = merge (msort fh) (msort sh)
           where (fh, sh) = halve xs