data Nat = Zero | Succ Nat
           deriving Show

add :: Nat -> Nat -> Nat
add Zero n = n
add m Zero = m -- for efficiency reasons
add (Succ m) n = Succ (add m n)

mul :: Nat -> Nat -> Nat
mul Zero _ = Zero
mul _ Zero = Zero
mul (Succ n) m = add m (mul n m)