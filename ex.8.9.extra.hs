data Expr = Val Int | Add Expr Expr | Mul Expr Expr

type Cont = [Op]

data Op = EVAL SOp Expr | EXEC SOp_partial

type SOp = Int -> Int -> Int
type SOp_partial = Int -> Int

value :: Expr -> Int
value e = eval e []

eval :: Expr -> Cont -> Int
eval (Val n) c = exec c n
eval (Add x y) c = eval x (EVAL (+) y : c)
eval (Mul x y) c = eval x (EVAL (*) y : c)

-- one could say that this is a security risk because op can be almost any function acting on integers and returning them
-- A safer alternative would be to ensure that such operations are in fact authorized.
-- Either through a type system but I have no idea how or by signing operations or by controlling who may pass exec is stack and who can modify this control stack
exec :: Cont -> Int -> Int
exec [] n = n
exec (EVAL op y : c) n = eval y (EXEC (op n) : c)
exec (EXEC op : c) n = exec c (op n)