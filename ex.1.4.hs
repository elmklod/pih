qsort [] = []
qsort (x:xs) = qsort larger ++ [x] ++ qsort smaller   -- switch larger and smaller
               where
                  smaller = [a | a <- xs, a <= x]
                  larger = [b | b <- xs, b > x]