isChoice :: Eq a => [a] -> [a] -> Bool
isChoice [] _ = True
isChoice (x:xs) [] = False
isChoice xs (y:ys) = isChoice (remove xs) ys
                     where
                        remove [] = []
                        remove (z:zs) | y == z = zs
                                      | otherwise = z : (remove zs)