bools = [] :: [Bool] -- or [True]

nums = [[] :: [Int]] -- or [[1:: Int, 3]]

add x y (z :: Int)  = x + y + z

copy x = (x, x)

apply (f :: a -> b) = f -- apply f x = f x

-- type declarations are added to constrain polymorphic types to certain ones