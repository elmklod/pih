putRow :: Int -> Int -> IO ()
putRow row num = do putStr (show row)
                    putStr ": "
                    putStrLn (concat (replicate num "* "))

type Board = [Int]

putBoard :: Board -> IO ()
putBoard xs = do putChar '\n'
                 rec 1 xs
              where
                 rec n [] = return ()
                 rec n (x:xs) = do putRow n x
                                   rec (n + 1) xs
-- instead of a number an infinite list [1..] could be passed to extract row number by pattern matching but this would be unnecessary and only create more work for gc