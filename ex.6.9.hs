mysum :: Num a => [a] -> a
mysum [] = 0
mysum (n:ns) = n + mysum ns

mytake :: Int -> [a] -> [a]
mytake 0 _ = []
mytake _ [] = []
mytake n (x:xs) = x : (mytake (n - 1) xs)

mylast :: [a] -> a
mylast [x] = x
mylast (_:xs) = mylast xs