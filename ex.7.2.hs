all :: (a -> Bool) -> [a] -> Bool
all p = foldr ((&&) . p) True -- foldr istead of foldl allows it to stop once the False element is encountered

any :: (a -> Bool) -> [a] -> Bool
any p = foldr ((||) . p) False

takeWhile :: (a -> Bool) -> [a] -> [a]
takeWhile p = foldr (\x -> if p x then (x:) else const []) [] -- (\_ -> []) instead of const

dropWhile :: (a -> Bool) -> [a] -> [a]
dropWhile p ys = foldl (\(_:xs) _ -> xs) ys (Main.takeWhile p ys)