sumdown :: Int -> Int
sumdown 0 = 0
sumdown n = n + sumdown (n-1) -- | n > 0 = ...
                              -- | otherwise = n