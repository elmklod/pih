import GHC.Read

data Mockery = Tockery | Bockery deriving Show

instance Read Mockery where
    readPrec
      = parens
          (choose
             [("False", return Tockery),
              ("True", return Bockery)])
    readList = readListDefault
    readListPrec = readListPrecDefault