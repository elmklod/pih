data Tree a = Leaf a | Node (Tree a) (Tree a)
              deriving Show

balanced :: Tree a -> Bool
--balanced (Leaf _) = True -- unnecessary
balanced t = case check t of
                Nothing -> False
                Just _ -> True
             where
                check :: Tree b -> Maybe Int -- b since there is no capture of a
                check (Leaf _) = Just 1
                check (Node l r) = case check l of
                                      Nothing -> Nothing
                                      Just x -> case check r of
                                                   Nothing -> Nothing
                                                   Just y -> if abs (x - y) > 1 then Nothing else Just (x + y)



balanced' :: Tree a -> Bool
--balanced' (Leaf _) = True -- unnecessary
balanced' t = case check t of
                Nothing -> False
                Just _ -> True
             where
                check :: Tree b -> Maybe Int
                check (Leaf _) = Just 1
                check (Node l r) | Just x <- check l, Just y <- check r = if abs (x - y) > 1 then Nothing else Just (x + y)
                                 | otherwise = Nothing


balanced'' :: Tree a -> Bool
balanced'' (Leaf _) = True
balanced'' (Node l r) = ((abs (count_leaves l - count_leaves r)) <= 1) && balanced l && balanced r

count_leaves :: Tree a -> Int
count_leaves (Leaf _) = 1
count_leaves (Node l r) = count_leaves l + count_leaves r
    
balance :: [a] -> Tree a
balance [x] = Leaf x
balance (x:xs) = Node (balance l) (balance r)
                 where
                    halve ys = (take n ys, drop n ys)
                               where n = (length ys) `div` 2
                    (l, r) = halve (x:xs)