luhnDouble :: Int -> Int
luhnDouble n = m - (9*(m `div` 10))
               where m = n*2

-- if m > 9 then (m - 9) else m
-- where m = n * 2

luhn :: Int -> Int -> Int -> Int -> Bool
luhn a b c d = ((luhnDouble a + b + luhnDouble c + d) `mod` 10) == 0