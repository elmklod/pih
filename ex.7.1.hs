mapfilter :: (a -> b) -> (a -> Bool) -> [a] -> [b]
mapfilter f p = map f . filter p
-- mapfilter f p xs
-- (map f . filter p) xs
-- [f x | x <- xs, p x] == map f (filter p xs)