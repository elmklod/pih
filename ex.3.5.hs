{-
Let's state the main way to say two functions are equal:
    Under the same cirmustances, they produce equal results.
    And for every possible combination of circumstances this holds

Since Haskell is pure(assume it!), the circumstances are inputs.

Therefore, two functions in haskell are equal iff:
    for any input a, the result of the first function applied to a
    is equal to the result of the second function applied to a.

due to the way == in Haskell works, functions are forced to have the same type for equality check.


It greatly simplyfies the check, but not enough.

There are many functions that act on infinite or extremely large domains.

How to check that for EVERY input the results are equal?

First - go through every possible input and check mechanically

Drawbacks: 1.Slow for very large domains of input
           2.Impossible for infinite domains of input

The only other way is to PROVE without going through every possible input in domain
that under the same input, the outputs are equal.

That's what maths people have been doing for centuries and they still haven't figured out how to prove it
for all domains.
Where they can prove it, it sometimes requires insight and/or tedious proving technique

Therefore, == for functions has to be bundled with a human mathematian or at least have some built-in non-trivial theorem-prover.
The first option would be very slow

Unfortunately, this non-trivial theorem prover is not included and its existence is in question
and Haskell still does not pay human mathematicans.


Therefore, there is no general function equlity predicate.


But in some cases, the equality check is possible and really cheap.
-}