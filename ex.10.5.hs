{- adder :: IO ()
adder = do putStr "\nHow many numbers? "
           ds <- getLine
           ms <- let n = readInt ds in sequence (replicate n (getLine >>= (\ds -> return (readInt ds))))        
           putStrLn ("The total is " ++ show (sum ms))
        where
           readInt :: String -> Int
           readInt = read -}

adder :: IO ()
adder = do putStr "\nHow many numbers? "
           ns <- getLine
           ms <- sequence (replicate (read ns) (do {ds <- getLine; return (read ds)}))        
           putStrLn ("The total is " ++ show (sum ms))