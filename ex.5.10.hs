import Data.Char


let2int :: Char -> Char -> Int
let2int c fstl = ord c - ord fstl

int2let :: Int -> Char -> Char
int2let n fstl = chr (ord fstl + n)

shift :: Int -> Char -> Char
shift n c | (isLower c || isUpper c) = int2let ((let2int c fstl + n) `mod` 26) fstl
          | otherwise = c
          where fstl | isLower c = 'a'
                     | isUpper c = 'A'
                     | otherwise = ' '

encode :: Int -> String -> String
encode n xs = [shift n x| x <- xs]

table :: [Float]
table = [8.1, 1.5, 2.8, 4.2, 12.7, 2.2, 2.0, 6.1, 7.0, 0.2, 0.8, 4.0, 2.4, 6.7, 7.5, 1.9, 0.1, 6.0, 6.3, 9.0, 2.8, 1.0, 2.4, 0.2, 2.0, 0.1]

percent :: Int -> Int -> Float
percent n m = (fromIntegral n / fromIntegral m) * 100

lowers_and_uppers :: String -> Int
lowers_and_uppers xs = length [x | x <- xs, isLower x || isUpper x]

count :: Char -> String -> Int
count x xs = length [x' | x' <- xs, x' == x]

freqs :: String -> [Float]
freqs xs = [percent ((count lower xs) + (count upper xs)) n | (lower, upper) <- (zip ['a'..'z'] ['A'..'Z'])]
           where n = lowers_and_uppers xs

chisqr :: [Float] -> [Float] -> Float
chisqr os es = sum [((o - e)^2)/e | (o, e) <- zip os es]

rotate :: Int -> [a] -> [a]
rotate n xs = drop n xs ++ take n xs

rotate_right :: Int -> [a] -> [a]  -- just because
rotate_right n xs = drop m xs ++ take m xs
                    where m = length xs - n

positions :: Eq a => a -> [a] -> [Int]
positions x xs = [n | (x',n) <- zip xs [0..], x == x']

crack :: String -> String
crack xs = encode (-factor) xs  -- with rotate_right -factor becomes factor
           where
              factor = head (positions (minimum chitab) chitab)
              chitab = [chisqr (rotate n table') table | n <- [0..25]] -- can use rotate_right here
              table' = freqs xs 
