data Expr = Val Int | Add Expr Expr | Mul Expr Expr

type Cont = [Op]

-- This approch generalizes the idea of deciding which operation to use after the first argument is evaluated

data Op = EVAL SOp Expr | ADD Int | MUL Int

type SOp = Int -> Op
-- also a security risk that was mentioned in extra, the only reliable way to remove this risk is to pattern match upon op, explicitly expressing which values of op are expected but that is not possible is it. Then the same approach as in extra

value :: Expr -> Int
value e = eval e []

eval :: Expr -> Cont -> Int
eval (Val n) c = exec c n
eval (Add x y) c = eval x (EVAL ADD y : c)
eval (Mul x y) c = eval x (EVAL MUL y : c)

exec :: Cont -> Int -> Int
exec [] n = n
exec (EVAL op y : c) n = eval y (op n : c)
exec (ADD n : c) m = exec c (n+m)
exec (MUL n : c) m = exec c (n*m)