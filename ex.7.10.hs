altMap :: (a -> b) -> (a -> b) -> [a] -> [b]
altMap _ _ [] = []
altMap f g (x:xs) = f x : altMap g f xs


luhnDouble :: Int -> Int
luhnDouble n = m - (9*(m `div` 10))
               where m = n*2

-- if m > 9 then (m - 9) else m
-- where m = n * 2

--luhn :: Int -> Int -> Int -> Int -> Bool
--luhn a b c d = ((luhnDouble a + b + luhnDouble c + d) `mod` 10) == 0


-- reverse may generate unnecessary list
-- the other way is to check length, if odd, then everything is the same without reverse
-- if even, id is swapped with luhnDouble
luhn :: [Int] -> Bool
luhn = (== 0) . (`mod` 10) . sum . (altMap id luhnDouble) . reverse