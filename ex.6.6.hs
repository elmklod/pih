myand :: [Bool] -> Bool
myand [] = True
myand (x:xs) = x && myand xs

myconcat :: [[a]] -> [a]
myconcat [] = []
myconcat (xs:xss) = xs ++ myconcat xss

myreplicate :: Int -> a -> [a]
myreplicate 0 _ = []
myreplicate n x = x : myreplicate (n - 1) x

(!!!) :: [a] -> Int -> a
(x:_) !!! 0 = x
(_:xs) !!! n = xs !!! (n - 1)

myelem :: Eq a => a -> [a] -> Bool
myelem _ [] = False
myelem x (y:ys) = (x == y) || myelem x ys