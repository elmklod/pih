halve :: [a] -> ([a], [a])
halve xs = (take lh xs, drop lh xs)
           where
              lh = length xs `div` 2