import System.IO

getCh :: IO Char
getCh = do hSetEcho stdin False
           x <- getChar
           hSetEcho stdin True
           return x

readUnproccessedLine :: IO String
readUnproccessedLine = do c <- getCh;
                          case c of
                             '\n' -> return []
                             _ -> do {if c == '\DEL' then back else putChar c; s <- readUnproccessedLine; return (c <:> s)}
                       where back = do putChar '\b'
                                       putChar ' '
                                       putChar '\b'
                             c@'\DEL' <:> s = c:s
                             _ <:> ('\DEL':s) = s
                             c <:> s = c:s

readLine :: IO String
readLine = do us <- readUnproccessedLine
              return (dropWhile ('\DEL'==) us)

main :: IO ()
main = do hSetBuffering stdout NoBuffering
          hSetBuffering stdin NoBuffering
          s <- readLine
          putStr s