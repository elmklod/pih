mylength :: [a] -> Int
mylength [] = 0
mylength (_:xs) = 1 + mylength xs

mydrop :: Int -> [a] -> [a]
mydrop 0 xs = xs
mydrop _ [] = []
mydrop n (_:xs) = mydrop (n-1) xs

myinit :: [a] -> [a]
myinit [_] = []
myinit (x:xs) = x : myinit xs

-- mylength [1,2,3] = 1 + mylength [2,3] = 1 + (1 + mylength [3]) = 1 + (1 + (1 + mylength [])) = 1 + (1 + (1 + 0)) = 3
-- mydrop 3 [1,2,3,4,5] = mydrop (3 - 1) [2,3,4,5] = mydrop 2 [2,3,4,5] = mydrop (2 - 1) [3,4,5] = mydrop 1 [3,4,5] = mydrop (1 - 1) [4,5] = mydrop 0 [4,5] = [4,5]
-- myinit [1,2,3] = 1 : myinit [2,3] = 1 : (2 : myinit [3]) = 1 : (2 : []) = 1 : [2] = [1,2]