(||) :: Bool -> Bool -> Bool
False || False = False
False || True = True
True || False = True
True || True = True

False || False = False
_ || _ = True

False || b = b
True || _ = True

b || c | b == c = b
       | otherwise = True