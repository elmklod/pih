import Data.Char

type Bit = Int

bin2int :: [Bit] -> Int
bin2int = foldr (\x y -> x + 2*y) 0

unfold :: (a -> Bool) -> (a -> b) -> (a -> a) -> a -> [b]
unfold p h t x | p x = []
               | otherwise = h x : unfold p h t (t x)

map :: (a -> b) -> [a] -> [b]
map f = unfold null (f . head) tail

int2bin :: Int -> [Bit]
int2bin = unfold (== 0) (`mod` 2) (`div` 2)

make8 :: [Bit] -> [Bit]  -- or remake it into make 9
make8 bits = take 8 (bits ++ repeat 0)

encode :: String -> [Bit]
encode = concat . Main.map (addparity . make8 . int2bin . ord) -- or make9 and addparity can be composed right before make9 since it only adds 0 never changing the parity

addparity :: [Bit] -> [Bit]
addparity bs = (sum bs `mod` 2) : bs

chop9 :: [Bit] -> [[Bit]]
chop9 = unfold null (take 9) (drop 9)

decode :: [Bit] -> String
decode = Main.map (chr . bin2int . check_and_remove_parity) . chop9

check_and_remove_parity :: [Bit] -> [Bit]
check_and_remove_parity (b:bs) | b == (sum bs `mod` 2) = bs
                               | otherwise = error "Parity bit does not match"

transmit :: String -> String
transmit = decode . channel . encode

channel :: [Bit] -> [Bit]
channel = tail