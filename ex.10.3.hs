putRow :: Int -> Int -> IO ()
putRow row num = do putStr (show row)
                    putStr ": "
                    putStrLn (concat (replicate num "* "))

type Board = [Int]

putBoard :: Board -> IO ()
putBoard xs = sequence_ ((putChar '\n'):[putRow n x | (n,x) <- zip [1..] xs])