sum [] = 0
sum (n:ns) = n + sum ns

-- sum [x] == sum (x:[]) = x + sum [] = x + 0 = x