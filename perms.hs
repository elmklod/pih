perms :: [a] -> Int -> [[a]]
perms cs 0 = [[]]
perms [] n = [[]] -- not sure about this
perms cs n = concat [map (c:) ps | c <- cs]
             where ps = perms cs (n - 1)